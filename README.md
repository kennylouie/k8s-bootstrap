# Kubernetes Bootstrap

## Introduction

Scripts used to bootstrap the infrastructure needed to run kubeadm.

## Design

```           +----------+
+-------------|          |--------------------------------------------+
| AWS Cloud   |    IG    |                                            |
|             +-------^--+                                            |
|                     |                                               |
|   +-----------------+-------------------------------------------+   |
|   | VPC +ELASTIC IP | +KEY PAIR                                 |   |
|   |                 |                                           |   |
|   | +---------------+----+  +-------------------------------+   |   |
|   | | ROUTE TABLE   |    |  | ROUTE TABLE                   |   |   |
|   | |   +---------+-+    |  |    +----------++-----------+  |   |   |
|   | |   | BASTION |   +-------+  |  MASTER  ||   WORKER1 |  |   |   |
|   | |   |  EC2    |   | NAT   <--+   EC2    ||    EC2    |  |   |   |
|   | |   | + SSH SG|   |       |  | + SSH SG || + SSH SG  |  |   |   |
|   | |   +---------+   +-------+  +----------++-----------+  |   |   |
|   | |                    |  |                  + WORKER2    |   |   |
|   | | PUBLIC SUBNET      |  | PRIVATE SUBNET      ...       |   |   |
|   | +--------------------+  +-------------------------------+   |   |
|   |                                                             |   |
|   +-------------------------------------------------------------+   |
|                                                                     |
+---------------------------------------------------------------------+
```

## Usage

0. Requires the aws cli with credentials permissive to ec2 commands, e.g. AmazonEC2FullAccess

1. Run the deploy-vpc.sh script to stand up underlying vpc, subnets, internet gateway, nat gateway, route tables, and routes as described in the design above:

```
./deploy-vpc.sh
```

At this point, a results.txt file is created with the respective ids of created infra. This is used in subsequent scripts to reference the correct resource ids. This can also be used to delete any infra later on.

2. Run deploy-sg.sh to create a security group:

```
./deploy-sg.sh -n bastion -d "for he bastion"
./deploy-sg.sh -n nodes -d "for the nodes"
```

3. Run deploy-key-pair.sh to create a key pair:

```
./deploy-key-pair.sh
```

At this point a k8s.pem file is created in the same directory.

4. Run deploy-ec2.sh to create the server instances required:

```
./deploy-ec2.sh -n BASTION -s bastion -p #-s is used to name the sg being used and -p is a flag to associate the ec2 with the public subnet
./deploy-ec2.sh -n MASTER -s nodes
./deploy-ec2.sh -n WORKER1 -s nodes
./deploy-ec2.sh -n WORKER2 -s nodes
```

5. Run cleanup.sh to cleanup the ec2 instances and vpc (with all its dependencies)

```
./cleanup.sh
```
