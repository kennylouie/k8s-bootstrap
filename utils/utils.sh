#!/bin/bash

set -u
set -e

say() {
	printf '[installer]: %s\n' "$1"
}

err() {
	say "error: $1" >&2
	exit 1
}

check_cmd() {
	command -v "$1" > /dev/null 2>&1
}

need_cmd() {
	if ! check_cmd "$1"; then
	    err "need '$1' (command not found)"
	fi
}

assert_file() {
	if [ ! -f "$1" ]; then say "$1 does not exist" >&2; return 1; fi
}

exp() {
	echo "export $1=$2" >> $3
}

exp_results()
{
	exp $1 $2 $RESULTS_FILE
}

utils_imported() {
	return 0
}
