#!/bin/bash

set -u
set -e

# required imports
source ../utils/utils.sh
[ ! $(command -v utils_imported) ] && err "utils not imported!"

RESULTS_FILE=results.txt

TAG_KEY=project
TAG_VALUE=tester

main()
{
	assert_file $RESULTS_FILE
	if [ $? != 0 ]; then err "cannot proceed without $RESULTS_FILE that was previously generated from other deploy scripts"; fi

	source $RESULTS_FILE

	say "creating aws security group..."
	need_cmd aws
  need_cmd getopts
  local _sg_name=""
  local _sg_description=""

  while getopts "n:d:" options; do
    case $options in
      n)
      _sg_name=$OPTARG
      ;;
      d)
      _sg_description=$OPTARG
      ;;
    esac
  done

  [ -z "${_sg_name}" ] && err "-n need to set name of security group"
  [ -z "${_sg_description}" ] && err "-d need to set description of security group"

	local _sg_output=$(aws ec2 create-security-group --group-name "${_sg_name}" --description "${_sg_description}" --vpc-id $VPC_ID)
	if [ $? != 0 ]; then err "security group create failed: ${_sg_output}"; fi

	local _sg_group_id=$(echo $_sg_output | jq -r ".GroupId")
	say "security group created with id $_sg_group_id"
	exp_results "SECURITY_GROUP_${_sg_name}_ID" $_sg_group_id

	# add tags
	aws ec2 create-tags --resource $_sg_group_id --tags Key=$TAG_KEY,Value=$TAG_VALUE
	if [ $? != 0 ]; then err "failed to add tags to security group $_sg_group_id"; fi
	say "added tags to security group $_sg_group_id"

	# add ssh access
	aws --no-cli-pager ec2 authorize-security-group-ingress --group-id $_sg_group_id --protocol tcp --port 22 --cidr 0.0.0.0/0

	say "security group created"
}

main "$@" || exit 1
