#!/bin/bash

set -u
set -e

source ../utils/utils.sh
[ ! $(command -v utils_imported) ] && err "utils not imported!"

RESULTS_FILE=results.txt

TAG_KEY=project
TAG_VALUE=tester

# ubuntu image
AMI_ID=ami-0530ca8899fac469f
INSTANCE_TYPE=t2.medium
COUNT=1
KEY_PAIR_NAME="tester"

main()
{
  assert_file $RESULTS_FILE
  if [ $? != 0 ]; then err "cannot proceed without $RESULTS_FILE that was previously generated from other deploy scripts"; fi

  source $RESULTS_FILE

  need_cmd aws
  need_cmd getopts
  local _instance_name=""
  local _subnet_type=$SUBNET_PRIVATE_ID
  local _sg_name=""
  local _public_flag=""

  while getopts "n:s:p" options; do
    case $options in
      n)
      _instance_name=$OPTARG
      ;;
      s)
      _sg_name=$OPTARG
      ;;
      p)
      _subnet_type=$SUBNET_PUBLIC_ID
      _public_flag="--associate-public-ip-address"
      ;;
    esac
  done

  [ -z "${_instance_name}" ] && err "-n need to set name of instance"
  [ -z "${_sg_name}" ] && err "-s need to set name of security group"
  _sg_id=SECURITY_GROUP_${_sg_name}_ID

  say "creating ec2 instance for $_instance_name in $_subnet_type with security group name ${_sg_name}..."

  _ec2_output=$(aws --no-cli-pager ec2 run-instances --image-id $AMI_ID --instance-type $INSTANCE_TYPE --count $COUNT --subnet-id $_subnet_type --security-group-ids ${!_sg_id} --key-name "${KEY_PAIR_NAME}" $_public_flag)
  if [ $? != 0 ]; then err "ec2 instances failed to create: ${_ec2_output}"; fi
  local _ec2_id=$(echo $_ec2_output | jq -r '.Instances[] | {InstanceId} | join(" ")')
  say "created ec2 instance with id $_ec2_id"

  exp_results "EC2_${_instance_name}_ID" $_ec2_id

  ## add tags
  aws ec2 create-tags --resource $_ec2_id --tags Key=$TAG_KEY,Value=$TAG_VALUE
  if [ $? != 0 ]; then err "failed to add tags to $_ec2_id"; fi
  say "added tags to ec2 instance $_ec2_id"

  say "ec2 instance created"
}

main "$@" || exit 1
