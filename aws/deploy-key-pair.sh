#!/bin/bash

set -u
set -e

# required imports
source ../utils/utils.sh
[ ! $(command -v utils_imported) ] && err "utils not imported!"

KEY_PAIR_NAME="tester"
PEM_FILE_PATH="$(pwd)/k8s.pem"

main()
{
  say "creating key pair..."
  need_cmd aws
  need_cmd chmod

  aws ec2 create-key-pair --key-name "${KEY_PAIR_NAME}" --query 'KeyMaterial' --output text > "${PEM_FILE_PATH}"
  if [ $? != 0 ]; then err "key pair failed to create"; fi

  chmod 400 ${PEM_FILE_PATH}

  say "key pair created"
}

main "$@" || exit 1
