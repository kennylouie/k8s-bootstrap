#!/bin/bash

# Script used to bootstrap the following:
# aws vpc
# aws vpc subnets
# aws internet gateway
# aws elastic ip
# aws nat gateway
# aws route tables
#
# All resources created in aws will be
# tagged with $TAG_KEY and $TAG_VALUE.
#
# Output written to $RESULTS_FILE.

set -u
set -e

# required imports
source ../utils/utils.sh
[ ! $(command -v utils_imported) ] && err "utils not imported!"

RESULTS_FILE=results.txt

TAG_KEY=project
TAG_VALUE=tester

VPC_CIDR="10.0.0.0/16"
SUBNET_PUBLIC_CIDR="10.0.1.0/24"
SUBNET_PRIVATE_CIDR="10.0.2.0/24"

main()
{
	need_cmd aws
	need_cmd jq
	
	# create vpc
	say "creating vpc..."
	_vpc_output=$(aws --no-cli-pager ec2 create-vpc --cidr-block $VPC_CIDR)
	if [ $? != 0 ]; then err "vpc failed to create: ${_vpc_output}"; fi
	
	local _vpc_id=$(echo $_vpc_output | jq -r ".Vpc.VpcId")
	say "created vpc with id $_vpc_id"
	exp_results "VPC_ID" $_vpc_id
	
	## add tags
	aws ec2 create-tags --resource $_vpc_id --tags Key=$TAG_KEY,Value=$TAG_VALUE
	if [ $? != 0 ]; then err "failed to add tags to $_vpc_id"; fi
	say "added tags to vpc $_vpc_id"
	
	# create public and private subnets
	## public subnet
	say "creating public subnet..."
	local _public_subnet_output=$(aws ec2 create-subnet --vpc-id $_vpc_id --cidr-block $SUBNET_PUBLIC_CIDR)
	if [ $? != 0 ]; then err "public subnet failed to create: ${_public_subnet_output}"; fi
	
	local _subnet_public_id=$(echo $_public_subnet_output | jq -r ".Subnet.SubnetId")
	say "created public subnet with id $_subnet_public_id"
	exp_results "SUBNET_PUBLIC_ID" $_subnet_public_id
	
	## add tags
	aws ec2 create-tags --resource $_subnet_public_id --tags Key=$TAG_KEY,Value=$TAG_VALUE
	if [ $? != 0 ]; then err "failed to add tags to public subnet $_subnet_public_id"; fi
	say "added tags to public subnet $_subnet_public_id"
	
	## private subnet
	say "creating private subnet..."
	local _private_subnet_output=$(aws ec2 create-subnet --vpc-id $_vpc_id --cidr-block $SUBNET_PRIVATE_CIDR)
	if [ $? != 0 ]; then err "private subnet failed to create: ${_private_subnet_output}"; fi
	
	local _subnet_private_id=$(echo $_private_subnet_output | jq -r ".Subnet.SubnetId")
	say "created private subnet with id $_subnet_private_id"
	exp_results "SUBNET_PRIVATE_ID" $_subnet_private_id
	
	## add tags
	aws ec2 create-tags --resource $_subnet_private_id --tags Key=$TAG_KEY,Value=$TAG_VALUE
	if [ $? != 0 ]; then err "failed to add tags to private subnet $_subnet_private_id"; fi
	say "added tags to private subnet $_subnet_private_id"
	
	# create internet gateway
	say "creating internet gateway..."
	local _internet_gateway_output=$(aws ec2 create-internet-gateway)
	if [ $? != 0 ]; then err "internet gateway failed to create: ${_internet_gateway_output}"; fi
	
	local _internet_gateway_id=$(echo $_internet_gateway_output | jq -r ".InternetGateway.InternetGatewayId")
	say "created internet gateway with id $_internet_gateway_id"
	exp_results "INTERNET_GATEWAY" $_internet_gateway_id
	
	## add tags
	aws ec2 create-tags --resource $_internet_gateway_id --tags Key=$TAG_KEY,Value=$TAG_VALUE
	if [ $? != 0 ]; then err "failed to add tags to internet gateway $_internet_gateway_id"; fi
	say "added tags to internet gateway $_internet_gateway_id"
	
	## attach internet gateway to vpc
	aws ec2 attach-internet-gateway --internet-gateway-id $_internet_gateway_id --vpc-id $_vpc_id
	
	# create elastic IP for NAT gateway
	say "creating elastic IP..."
	local _elastic_ip_output=$(aws ec2 allocate-address --domain vpc)
	if [ $? != 0 ]; then err "elastic IP failed to create: ${_elastic_ip_output}"; fi
	
	local _elastic_ip_allocation_id=$(echo $_elastic_ip_output | jq -r ".AllocationId")
	say "created elastic ip with id $_elastic_ip_allocation_id"
	exp_results "ELASTIC_IP_ALLOCATION_ID" $_elastic_ip_allocation_id
	
	# create NAT gateway for public subnet
	say "creating NAT gateway..."
	local _nat_gateway_output=$(aws ec2 create-nat-gateway --subnet-id $_subnet_public_id --allocation-id $_elastic_ip_allocation_id)
	if [ $? != 0 ]; then err "NAT gateway failed to create: ${_nat_gateway_output}"; fi
	
	local _nat_gateway_id=$(echo $_nat_gateway_output | jq -r ".NatGateway.NatGatewayId")
	say "created NAT gateway with id $_nat_gateway_id"
	exp_results "NAT_GATEWAY_ID" $_nat_gateway_id
	
	## add tags
	aws ec2 create-tags --resource $_nat_gateway_id --tags Key=$TAG_KEY,Value=$TAG_VALUE
	if [ $? != 0 ]; then err "failed to add tags to NAT gateway $_nat_gateway_id"; fi
	say "added tags to NAT gateway $_nat_gateway_id"
	
	# create route tables
	say "creating route tables..."
	
	## public
	local _route_table_public_output=$(aws ec2 create-route-table --vpc-id $_vpc_id)
	if [ $? != 0 ]; then err "public route table failed to create: ${_route_table_public_output}"; fi
	
	local _route_table_public_id=$(echo $_route_table_public_output | jq -r ".RouteTable.RouteTableId")
	say "created public route table with id $_route_table_public_id"
	exp_results "ROUTE_TABLE_PUBLIC_ID" $_route_table_public_id
	
	## add tags
	aws ec2 create-tags --resource $_route_table_public_id --tags Key=$TAG_KEY,Value=$TAG_VALUE
	if [ $? != 0 ]; then err "failed to add tags to public route table $_route_table_public_id"; fi
	aws ec2 create-tags --resource $_route_table_public_id --tags Key=subnet,Value=public
	if [ $? != 0 ]; then err "failed to add tags to public route table $_route_table_public_id"; fi
	say "added tags to public route table $_route_table_public_id"
	
	## private
	local _route_table_private_output=$(aws ec2 create-route-table --vpc-id $_vpc_id)
	if [ $? != 0 ]; then err "private route table failed to create: ${_route_table_private_output}"; fi
	
	local _route_table_private_id=$(echo $_route_table_private_output | jq -r ".RouteTable.RouteTableId")
	say "created private route table with id $_route_table_private_id"
	exp_results "ROUTE_TABLE_PRIVATE_ID" $_route_table_private_id
	
	## add tags
	aws ec2 create-tags --resource $_route_table_private_id --tags Key=$TAG_KEY,Value=$TAG_VALUE
	if [ $? != 0 ]; then err "failed to add tags to private route table $_route_table_private_id"; fi
	aws ec2 create-tags --resource $_route_table_private_id --tags Key=subnet,Value=private
	if [ $? != 0 ]; then err "failed to add tags to private route table $_route_table_private_id"; fi
	say "added tags to private route table $_route_table_private_id"
	
	# create routes
	say "creating routes..."
	
	## route public ipv4 traffic to public route table and the internet gateway
	aws ec2 create-route --route-table-id $_route_table_public_id --destination-cidr-block 0.0.0.0/0 --gateway-id $_internet_gateway_id
	say "created public route"
	
	## route public ipv4 traffic to NAT gateway enroute to the private route table
	aws ec2 create-route --route-table-id $_route_table_private_id --destination-cidr-block 0.0.0.0/0 --gateway-id $_nat_gateway_id
	say "created private route"
	
	# associating route tables with subnets
	say "associating route tables and subnets..."
	
	## associate public route table with public subnet
	local _public_association_output=$(aws ec2 associate-route-table --route-table-id $_route_table_public_id --subnet-id $_subnet_public_id)
	if [ $? != 0 ]; then err "public route association failed: ${_public_association_output}"; fi
	
	local _public_association_id=$(echo $_public_association_output | jq -r ".AssociationId")
	say "associated public route table with public subnet with association id $_public_association_id"
	exp_results "PUBLIC_ASSOCIATION_ID" $_public_association_id
	
	## associate private route table with private subnet
	local _private_association_output=$(aws ec2 associate-route-table --route-table-id $_route_table_private_id --subnet-id $_subnet_private_id)
	if [ $? != 0 ]; then err "private route association failed: ${_private_association_output}"; fi
	
	local _private_association_id=$(echo $_private_association_output | jq -r ".AssociationId")
	say "associated private route table with private subnet with association id $_private_association_id"
	exp_results "PRIVATE_ASSOCIATION_ID" $_private_association_id
	
	say "vpc stack created"
}

main "$@" || exit 1
