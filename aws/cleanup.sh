#!/bin/bash

# Cleanup all resources in aws

set -u
set -e

# required imports
source ../utils/utils.sh
[ ! $(command -v utils_imported) ] && echo "utils not imported!" && exit 1

RESULTS_FILE=results.txt

detach_internet_gateway()
{
  aws ec2 detach-internet-gateway --internet-gateway-id $1 --vpc-id $2
}

delete_internet_gateway()
{
  aws ec2 delete-internet-gateway --internet-gateway $1
}

delete_nat_gateway()
{
  aws ec2 delete-nat-gateway --nat-gateway-id $1
}

delete_elastic_ip()
{
  aws ec2 release-address --allocation-id $1
}

delete_vpc()
{
  aws ec2 delete-vpc --vpc-id $1
}

delete_sg()
{
  aws ec2 delete-security-group --group-id $1
}

delete_ec2()
{
  aws --no-cli-pager ec2 terminate-instances --instance-ids $1
}

delete_subnets()
{
  aws ec2 delete-subnet --subnet-id $1
}

delete_route_table()
{
  aws ec2 delete-route-table --route-table-id $1
}

main()
{
  assert_file $RESULTS_FILE
  if [ $? != 0 ]; then err "cannot proceed without $RESULTS_FILE that was previously generated from other deploy scripts"; exit 1; fi

  source $RESULTS_FILE

  delete_ec2 $EC2_ID
  delete_nat_gateway $NAT_GATEWAY_ID
  delete_sg $SECURITY_GROUP_ID
  delete_elastic_ip $ELASTIC_IP_ALLOCATION_ID
  detach_internet_gateway $INTERNET_GATEWAY $VPC_ID
  sleep 5
  delete_internet_gateway $INTERNET_GATEWAY
  delete_subnets $SUBNET_PRIVATE_ID
  delete_subnets $SUBNET_PUBLIC_ID
  delete_route_table $ROUTE_TABLE_PRIVATE_ID
  delete_route_table $ROUTE_TABLE_PUBLIC_ID
  delete_vpc $VPC_ID

  say "finished."
}

main "$@" || exit 1
